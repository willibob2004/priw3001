#include <iostream>
#include <dummy.h>
#include <Init.h>
#include <Add.h>
#include <Commit.h>

int main(int argc, char *argv[])
{
    std::vector<std::string> args(argv + 1, argv + argc);

    if (args.size() == 0){
        std::cout << "Vous devez entrer un args" << std::endl;
    }
    else{
            if (args[0] == "--help") 
            {
                std::cout << "args:" << std::endl;
                std::cout << getHelpString() << std::endl;
            }
            else if (args[0] == "init") 
            {
                init(args);
            }
            else if (args[0] == "add") 
            {
                add(args);
            }
            else if (args[0] == "commit") 
            {
                commit(args);
            }
            else 
            {
                std::cout << "Entrer un argument valid" << std::endl;
            }
        }

    return 0;
}



