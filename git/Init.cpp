#include "Init.h"
#include <iostream>
#include <boost/filesystem.hpp>

const std::string REPO_NAME = ".git";
const std::string ERROR_TO_MANY_ARGS = "--init n'a pas besoin d'argument";
const std::string HELP_STRING = "  --init      Initialise .git dans votre repertoir";

std::string getHelpString()
{
    return HELP_STRING;
}

bool init(std::vector<std::string> args)
{
    if (std::find(args.begin(), args.end(), "--help") != args.end()) { std::cout << HELP_STRING; return false; }

    if (args.size() > 1)
    {
        std::cout << ERROR_TO_MANY_ARGS << std::endl;
    }
    else
    {
        try
        {
            if (!boost::filesystem::is_directory(REPO_NAME))
            {
                boost::filesystem::create_directory(REPO_NAME);              
                boost::filesystem::create_directory(REPO_NAME + "/objects"); 
                std::ofstream file1(REPO_NAME + "/index");
                file1.close();
                std::ofstream file2(REPO_NAME + "/HEAD");
                file2.close();
                std::cout << "Completer avec success" << std::endl;
            }
            else
            {
                boost::filesystem::remove_all(REPO_NAME);
                init(args);  
            }
        }

        catch (const boost::filesystem::filesystem_error& error)
        {
            if (error.code() == boost::system::errc::permission_denied)
            {
                std::cout << "Vous n'avez de pas permission d'ecrire" << std::endl;
            }
            else
            {
                std::cout << error.code();
            }

            return false;
        }
    }
    return true;
}
