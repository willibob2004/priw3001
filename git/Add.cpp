#include "Add.h"
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include "Hash.h"

const std::string INDEX = ".git/index"; 
const std::string STR_HELP = "usage: ./gitus add <pathspec>";
const std::string STR_ERROR = "./gitus : invalid option. \n Type \"./gitus add --help\" for more informations.";
const std::string BLOB = "blob";
const std::string OBJ_PATH = ".git/objects/";

bool add(std::vector<std::string> args)
{
    if (std::find(args.begin(), args.end(), "--help") != args.end()) 
    { 
        std::cout << STR_HELP; return false; 
    }

    if (args.size() > 2)
    {
        std::cout << STR_ERROR << std::endl;
        return false;
    }
    else
    {   
        std::string file = args[1];
        boost::filesystem::path objectPath(OBJ_PATH);
        boost::filesystem::path indexPath(INDEX);
        boost::filesystem::path p(file);
        std::string line;
        std::ifstream inFile(INDEX);
        std::ofstream tmpoFile;
        std::string tmpFile = ".git/tmp.txt"; 

        if (!boost::filesystem::is_regular_file(p)) 
        {
            std::cout << "the requested file does not exist" << std::endl; 
        }
        
        else if (!boost::filesystem::is_regular_file(indexPath) || !boost::filesystem::is_directory(OBJ_PATH))
        {
            std::cout << ".git/index or .git/objects/ is missing, we cannot proceed" << std::endl;
        }
        else
        {
            Hash::insertObject(file,BLOB);

            bool indexExist = false;
            tmpoFile.open(tmpFile);
            while (!inFile.eof())
            {
                getline(inFile, line);
                size_t found = line.substr(0, file.length()).find(file);
                if (found != std::string::npos)
                {
                    std::string content = Hash::getContent(file);
                    tmpoFile << file << " " << Hash::getContent(file).length() 
                    << " " << Hash::getSha1(content, BLOB);
                    indexExist = true;
                }
                else
                {
                    tmpoFile << line;
                }
                if (line.length() > 1)
                {
                    tmpoFile << std::endl;
                }
            }
            tmpoFile.close();
            if (indexExist)
            {
                remove(INDEX.c_str());
                rename(tmpFile.c_str(), INDEX.c_str());
            }
            else
            {
                std::ofstream file1(INDEX, std::ios::app);
                std::string content = Hash::getContent(file);
                file1 << file << " " << Hash::getContent(file).length() << " " 
                << Hash::getSha1(content, BLOB) << std::endl;
                file1.close();
                remove(tmpFile.c_str());
            }

            std::cout << "Ajouter" << file << std::endl;
            return true;
        }
    }
}
