#include "Commit.h"
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <Hash.h>

const std::string STR_HELP = "usage: gitus commit <msg> <author>";
const std::string STR_ERROR = "./gitus : invalid option. \n Type \"./gitus add --help\" for more informations.";
const std::string INDEX_PATH = ".git/index";
const std::string OBJ_PATH = ".git/objects/";
const std::string HEAD_PATH = ".git/HEAD";

bool commit(std::vector<std::string> arguments)
{
	if (std::find(arguments.begin(), arguments.end(), "--help") != arguments.end()) { std::cout << STR_HELP; return true; }

	if (arguments.size() != 3)
	{
		std::cout << STR_ERROR << std::endl;
		return false;
	}
	else if (!boost::filesystem::is_regular_file(INDEX_PATH.c_str()))
	{
		std::cout << ".git/index is missing, we cannot proceed";
		return false;
	}
	else if(Hash::getContent(INDEX_PATH).length() == 0){ 
		std::cout << ".git/index is empty no commit to do.";
		return false;
	}
	else if (!boost::filesystem::is_regular_file(HEAD_PATH.c_str()))
	{
		std::cout << ".git/HEAD is missing, we cannot proceed";
		return false;
	}
	else
	{
		TreeObject tree = createRootTreeFromIndex();
		std::string shaTree = tree.sha;
		std::string msg = arguments[1];
		std::string author = arguments[2];
		createCommit(shaTree, author, msg);
		std::cout << "Successfully committed changes" << std::endl;
		return true;
	}
	
}

TreeObject createRootTreeFromIndex()
{
	std::ifstream inFile(INDEX_PATH);
	std::string line;
	std::vector<TreeObject> indexVector;
	populateVectorFromPreviousTree(indexVector, getLastCommitTreeSha(), "");

	while (!inFile.eof())
	{
		getline(inFile, line);
		if (line != "") {
			indexVector.push_back(createTreeObjectFromIndex(line));
		}
	}

	return createTree(indexVector, "", "");
}

TreeObject createTreeObjectFromIndex(std::string line) {

	std::string name = line.substr(0, line.find(" "));
	line.erase(0, line.find(" ") + 1); 
	line.erase(0, line.find(" ") + 1); 
	std::string sha = line.substr(0, line.find(" "));
	TreeObject tree;
	tree.name = name;
	tree.sha = sha;
	tree.type = "blob";
	tree.path = "";

	while (tree.name.find("/") != std::string::npos) {
		tree.path += tree.name.substr(0, tree.name.find("/") + 1);
		tree.name.erase(0, tree.name.find("/") + 1);
	}

	return tree;
}

std::string getLastCommitTreeSha() {
	std::ifstream headFile(HEAD_PATH);
	std::string commitSha;
	getline(headFile, commitSha);
	if (commitSha == "") { return ""; }
	std::string path = Hash::getObjectPath(commitSha);

	std::ifstream commitFile(path);
	std::string line;
	std::string treeSha;
	while (!commitFile.eof())
	{
		getline(commitFile, line);
		if (line.find("tree") != std::string::npos) {
			treeSha = line.substr(5, line.size() - 1);
		}
	}
	return treeSha;
}

void populateVectorFromPreviousTree(std::vector<TreeObject>& vector, std::string treeSha, std::string currentPath) {
	if (treeSha == "") { return; }
	std::string treePath = Hash::getObjectPath(treeSha);
	std::ifstream commitFile(treePath);
	std::string line;
	while (!commitFile.eof())
	{
		getline(commitFile, line);
		if (line != "") {
			std::string mutableLine = line;
			TreeObject obj;
			std::string type = mutableLine.substr(0, mutableLine.find(" "));
			mutableLine.erase(0, mutableLine.find(" ") + 1);
			std::string sha = mutableLine.substr(0, mutableLine.find(" "));
			mutableLine.erase(0, mutableLine.find(" ") + 1);
			std::string name = mutableLine;
			obj.type = type;
			obj.sha = sha;
			obj.name = name;

			if (obj.type == "tree") {
				populateVectorFromPreviousTree(vector, sha, currentPath + obj.name + "/");
			}
			else if (obj.type == "blob") {
				obj.path = currentPath;
				vector.push_back(obj);
			}
		}
	}
}

TreeObject createTree(std::vector<TreeObject> indexVector, std::string exactPath, std::string currentFolder) {
	std::string content = "";
	TreeObject tree;
	for (int i = 0; i < indexVector.size(); i++) {
		if (indexVector[i].path == exactPath) {
			content += indexVector[i].type + " " + indexVector[i].sha + " " + indexVector[i].name + "\n";
		}
	}
	for (int i = 0; i < indexVector.size(); i++) {
		TreeObject subtree;
		std::string trimmedPath = indexVector[i].path; 
		if (trimmedPath.find(exactPath) != std::string::npos) {
			int pos = trimmedPath.find(exactPath);
			 trimmedPath.erase(0, exactPath.size()); 

			if (trimmedPath != "") { 
				trimmedPath.erase(trimmedPath.find("/") + 1, trimmedPath.size() - 1);
				std::string newPath = exactPath + trimmedPath;
				trimmedPath.pop_back(); // we remove the "/" from the folder name

				subtree = createTree(indexVector, newPath, trimmedPath);
				std::string newContent = subtree.type + " " + subtree.sha + " " + subtree.name + "\n";
				if (content.find(newContent) == std::string::npos) { content += newContent; }
			}
		}
	}
	if (exactPath.find(currentFolder) != std::string::npos) {
		exactPath.erase(exactPath.find(currentFolder), exactPath.size() - 1);
	}

	tree.name = currentFolder;
	tree.path = exactPath;
	tree.type = "tree";
	tree.sha = Hash::getSha1(content, tree.type);
	insertTreeObject(content, tree.type);
	return tree;
}



void insertTreeObject(std::string content, std::string type)
{
	std::string sha1 = Hash::getSha1(content, type);

	std::string folderName = sha1.substr(0, 2);
	boost::filesystem::path objectPath(OBJ_PATH + folderName);
	std::string newfileName = sha1.substr(2, sha1.length());
	std::string objectFilePath = OBJ_PATH + folderName + "/" + newfileName;

	if (!boost::filesystem::is_directory(objectPath))
	{
		boost::filesystem::create_directory(objectPath);
	}
	std::ofstream file1(objectFilePath);
	file1 << content;
	file1.close();
}

void createCommit(std::string shaTree, std::string author, std::string message){
	std::string parent = Hash::getContent(HEAD_PATH);
	std::string commitToHash;
	if( parent != "" ) {
		commitToHash = "tree " + shaTree + "\n" + "parent " + parent + "\n" + "author " + author + "\n" + message;
	}
	else {
		commitToHash = "tree " + shaTree + "\n" + "author " + author + "\n" + message;
	}
	
	
    std::string sha1 = Hash::getSha1(commitToHash, "commit");

    std::string folderName = sha1.substr(0, 2);
    boost::filesystem::path objectPath(OBJ_PATH + folderName);
    std::string newfileName = sha1.substr(2, sha1.length());
    std::string objectFilePath = OBJ_PATH + folderName + "/" + newfileName;

    if (!boost::filesystem::is_directory(objectPath))
    {
        boost::filesystem::create_directory(objectPath);
    }
    std::ofstream file1(objectFilePath);
    file1 << commitToHash;
    file1.close();

	remove(HEAD_PATH.c_str());

	std::ofstream headFile(HEAD_PATH);
	headFile << sha1;
	headFile.close();

	remove(INDEX_PATH.c_str());
	std::ofstream newEmptyIndex(INDEX_PATH);
	newEmptyIndex.close();
}