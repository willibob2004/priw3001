#pragma once
#include <vector>
#include <iostream>

struct TreeObject {
	std::string type;
	std::string name;
	std::string sha;
	std::string path;
};

bool commit(std::vector<std::string>);
void createCommit(std::string, std::string, std::string);

std::string getLastCommitTreeSha();
void populateVectorFromPreviousTree(std::vector<TreeObject>&, std::string, std::string);

TreeObject createRootTreeFromIndex();
TreeObject createTreeObjectFromIndex(std::string);
TreeObject createTree(std::vector<TreeObject>, std::string, std::string);
void insertTreeObject(std::string, std::string);