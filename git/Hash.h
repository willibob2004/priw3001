#pragma once
#include <string>

namespace Hash 
{
	std::string getSha1(std::string content,std::string objType);
	std::string getObjectPath(std::string sha);
	void insertObject(std::string fileName, std::string type);
	std::string getContent(std::string filePath);
};