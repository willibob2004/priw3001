#pragma once
#include "Hash.h"
#include <boost/uuid/detail/sha1.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/uuid/detail/sha1.hpp>

using boost::uuids::detail::sha1;
using std::ifstream;
using std::string;
const std::string OBJ_PATH = ".git/objects/";


std::string Hash::getObjectPath(std::string sha) {
	return OBJ_PATH + sha.substr(0, 2) + "/" + sha.substr(2, sha.size() - 1);
}

void Hash::insertObject( std::string fileName, std::string type)
{
	std::string content = Hash::getContent(fileName);
	std::string sha1_string = Hash::getSha1(content, type);

    std::string folderName = sha1_string.substr(0, 2);
    boost::filesystem::path objectPath(OBJ_PATH + folderName);
    std::string newfileName = sha1_string.substr(2, sha1_string.length());
    std::string objectFilePath = OBJ_PATH + folderName + "/" + newfileName;

    if (!boost::filesystem::is_directory(objectPath))
    {
        boost::filesystem::create_directory(objectPath); 
    }
    std::ofstream file1(objectFilePath); 

    file1  << fileName << " " << content.length() << " " << content;
    file1.close();
}

std::string Hash::getSha1(std::string content, std::string type)
{
	sha1 sha;
	std::string header;

	std::string new_content;
	header = type;
	header += " ";
	header += std::to_string(content.length());
	header += '\0';
	content = header + content;
	sha.process_bytes(content.c_str(), content.length());
	unsigned int hash[5];
	sha.get_digest(hash);
	std::stringstream stream;
	std::string result;
	for (int i = 0; i < 5; ++i)
	{
		stream << std::hex << hash[i];
	}
	return stream.str();
}

std::string Hash::getContent(std::string filePath)
{

	ifstream file(filePath);
	string content{std::istreambuf_iterator<char>(file),
				   std::istreambuf_iterator<char>()};
	return content;
}