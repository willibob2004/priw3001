#define CATCH_CONFIG_MAIN

#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>


// RTFM catch2:
// https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#top

#include "catch.hpp"
#include "Hash.h"
#include "Add.h"
#include "Init.h"
#include "Commit.h"

/*
	SCENARIOS
*/

void clearTestFiles()
{
	//delete files/folders
	boost::filesystem::path _path = "./a.txt";
	boost::filesystem::remove_all(_path);
	_path = "./z.txt";
	boost::filesystem::remove_all(_path);
	_path = "./a";
	boost::filesystem::remove_all(_path);
	//clear .git/ folder
	_path = "./.git";
	boost::filesystem::remove_all(_path);
}
void createTestFiles()
{
	//create files/folders
	std::ofstream file1( "./a.txt"); file1 << "hello 122"; file1.close();
	boost::filesystem::create_directory("./a"); 
	std::ofstream file2( "./a/b.txt"); file2 << "hello 133"; file2.close();
	boost::filesystem::create_directory("./a/b"); 
	std::ofstream file3( "./a/b/c.txt"); file3 << "hello 144"; file3.close();
	std::ofstream file4( "./a/b/d.txt"); file4 << "hello 155"; file4.close();
}

SCENARIO( "INIT Command" ) 
{
	clearTestFiles();
	
	GIVEN(".git does not exist")
	{	
		createTestFiles();

		std::vector<std::string> arguments;  // Empty init argument

		REQUIRE_NOTHROW( init(arguments) );

		//Check if the files and folder are there
		std::string gitPath(".git/");
        boost::filesystem::path indexPath(".git/index");
		boost::filesystem::path headerPath(".git/HEAD");
		std::string objectPath(".git/objects/");

		REQUIRE(boost::filesystem::is_directory(gitPath));
		REQUIRE(boost::filesystem::is_regular_file(indexPath));
		REQUIRE(boost::filesystem::is_regular_file(headerPath));
		REQUIRE(boost::filesystem::is_directory(objectPath));

		clearTestFiles();
	}
	
	GIVEN("re-initialise .git")
	{
		createTestFiles();

		std::vector<std::string> arguments;  // Empty init argument
		init(arguments);

		//Write something in the files so we can simulate an add
		std::ofstream indexFile( "./.git/index");indexFile << "beefSteak";indexFile.close();
		std::ofstream headerFile( "./.git/HEAD");headerFile << "chickenSteak";headerFile.close();
		boost::filesystem::create_directory(".git/objects/69"); //CREATE FOLDER objects

		REQUIRE( init(arguments) );
		//MAKE SURE everything is reset
		boost::filesystem::path indexPath(".git/index");
		boost::filesystem::path headerPath(".git/HEAD");
		std::string objectPath(".git/objects/69");

		REQUIRE(boost::filesystem::is_empty(indexPath));
		REQUIRE(boost::filesystem::is_empty(headerPath));
		REQUIRE(!boost::filesystem::is_directory(objectPath));

		clearTestFiles();
	}	
	
}

SCENARIO( "Add Command" ) 
{

		GIVEN("file to add do not exist")
		{
			createTestFiles();

			std::vector<std::string> arguments;
			arguments.push_back("add");
			arguments.push_back("doNotExist.txt");
			REQUIRE(!add(arguments));

			clearTestFiles();
		}	

		GIVEN("index file missing")
		{
			createTestFiles();

			std::vector<std::string> arguments;
			arguments.push_back("add");
			arguments.push_back("./a.txt");

			boost::filesystem::path _path = ".git/index";
			boost::filesystem::remove_all(_path);

			REQUIRE(!add(arguments));

			clearTestFiles();
		}

		GIVEN("objects folder missing")
		{
			createTestFiles();

			std::vector<std::string> arguments;
			arguments.push_back("add");
			arguments.push_back("./a.txt");

			boost::filesystem::path _path = ".git/objects";
			boost::filesystem::remove_all(_path);

			REQUIRE(!add(arguments));

			clearTestFiles();
		}

		GIVEN("all file their setup righth and are ready")
		{
			createTestFiles();
			std::vector<std::string> argumentsInit;  // Empty init argument
			init(argumentsInit);

			// ================== ADD ONE FILE =========================
			std::vector<std::string> arguments;
			arguments.push_back("add");
			arguments.push_back("./a.txt");
			//Recreate the index file
			std::ofstream file1( "./.git/index"); file1.close();
			//Run the command
			REQUIRE( add(arguments) );
			//Now if there is no error we shoud see a folder called 6e
			REQUIRE(boost::filesystem::is_directory("./.git/objects/ec"));

			boost::filesystem::path newFilePath("./.git/objects/ec/ed0b5151fb4f6d8b3b48eb49b86eec57ab564a");
			// Check if the correct object is create
			REQUIRE(boost::filesystem::is_regular_file(newFilePath));

			// ================== ADD ALL FILES =========================
			arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b.txt");
			REQUIRE_NOTHROW( add(arguments) );
			arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/c.txt");
			REQUIRE_NOTHROW( add(arguments) );
			arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/d.txt");
			REQUIRE_NOTHROW( add(arguments) );

		//===================  Check the index file ===================

			std::ifstream inFile("./.git/index");
			std::string line;
			bool goodIndex = true;
			int count = 0;
			std::string str;
			while (!inFile.eof())
			{
				getline(inFile, line);
				str = line;

				if(count == 0 && !str.compare("./a.txt 9 eced0b5151fb4f6d8b3b48eb49b86eec57ab564a\n")){ goodIndex = false;}
				if(count == 1 && !str.compare("./a/b.txt 9 7bb21c769ec894dc7c7001a1dc6bff130035bb0\n")){ goodIndex = false;}
				if(count == 2 && !str.compare("./a/b/c.txt 9 46cf08fa55f52bb36dc32ec1adb64527b71572ff\n")){ goodIndex = false;}
				if(count == 3 && !str.compare("./a/b/d.txt 9 45dc4ab5af160d54014969fd17d219a42cd1b4e\n")){ goodIndex = false;}

				count ++;
			}
			REQUIRE(goodIndex);

		clearTestFiles();
		}

}

SCENARIO( "Commit Command" ) 
{
	GIVEN("Wrong number of arguments")
	{	
		std::vector<std::string> arguments;
		arguments.push_back("add");
		arguments.push_back("doNotExist.txt");
		createTestFiles();

		std::vector<std::string> argumentsInit;  // Empty init argument
		init(argumentsInit);

		// ADD
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b.txt");
		REQUIRE( add(arguments) );
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/c.txt");
		REQUIRE( add(arguments) );
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/d.txt");
		REQUIRE( add(arguments) );

		// run commit with missing argument
		std::vector<std::string> argumentsCommit;
		argumentsCommit.push_back("commit");
		argumentsCommit.push_back("this is the first commit test");

		REQUIRE(!commit(arguments));

		clearTestFiles();
	}

	GIVEN("Wrong number of arguments too many")
	{	
		std::vector<std::string> arguments;
		arguments.push_back("add");
		arguments.push_back("doNotExist.txt");

		createTestFiles();
		std::vector<std::string> argumentsInit;  // Empty init argument
		init(argumentsInit);
		//Add
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b.txt");
		REQUIRE( add(arguments) );
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/c.txt");
		REQUIRE( add(arguments) );
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/d.txt");
		REQUIRE( add(arguments) );
		
		// Too much argument
		std::vector<std::string> argumentsCommit;
		argumentsCommit.push_back("commit");
		argumentsCommit.push_back("this is the first commit test");
		argumentsCommit.push_back("Will-OP");
		argumentsCommit.push_back("too much");
		REQUIRE(!commit(arguments));

		clearTestFiles();
	}

	GIVEN("all file their and are ready")
	{
		createTestFiles();
		std::vector<std::string> argumentsInit;  // Empty init argument
		init(argumentsInit);

		// ================== ADD ALL FILES =========================
		std::vector<std::string> arguments;
		arguments.push_back("add"); arguments.push_back("./a/b.txt");
		REQUIRE( add(arguments) );
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/c.txt");
		REQUIRE( add(arguments) );
		arguments.pop_back();arguments.pop_back(); arguments.push_back("add"); arguments.push_back("./a/b/d.txt");
		REQUIRE( add(arguments) );

		// HERE WE WILL ADDD ALL TEST FILES AND FOLDER THEN RUN THE COMMIT
		std::vector<std::string> argumentsCommit;
		argumentsCommit.push_back("commit");
		argumentsCommit.push_back("this is the first commit test");
		argumentsCommit.push_back("theTester");
		REQUIRE(commit(argumentsCommit));

		// ==================== READ THE HEAD FILE ========================
		std::ifstream inFile("./.git/HEAD");
		std::string line;
		bool goodHEAD = false;
		while (!inFile.eof())
		{
			getline(inFile, line);
			if (line == "816314db932773a58d0af179dbbc08428832632") {
				goodHEAD = true;
				break;
			}
		}
		REQUIRE(goodHEAD);

		// COMMIT A SECOND TIME

		// we add a new file
		std::ofstream file1( "./z.txt"); file1 << "hello 666"; file1.close();
		std::vector<std::string> argumentsNew;
		argumentsNew.push_back("add");
		argumentsNew.push_back("./z.txt");
		REQUIRE(add(argumentsNew));

		std::vector<std::string> arguments2;
		arguments2.push_back("commit");
		arguments2.push_back("this is the second commit test");
		arguments2.push_back("theTesterNumber2");
		REQUIRE(commit(arguments2));

		std::ifstream inFile2("./.git/HEAD");
		std::string line2;
		goodHEAD = false;
		while (!inFile2.eof())
		{
			getline(inFile2, line2);
			if (line2 == "63be86b04f8aa6351212d62c9858575fbf041724") {
				goodHEAD = true;
				break;
			}
		}

		REQUIRE(goodHEAD);

	}
}
